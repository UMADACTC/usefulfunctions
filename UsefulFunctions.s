@ Libreria con funciones utiles para imprimir por pantalla y leer de teclado en la 
@ el simulador ARMSim#
@
@ Uso con el simulador en ventana: 
@	Cargar con "File->Load multiple" el programa ".s" y el fichero UsefulFunctions.s
@	Ejecutar con el simulador el programa con las llamadas a la libreria
@
@ Functions:
@    tcprints    -- print a null-terminated ASCII string to standard output
@    tcprinti    -- print a integer number to standard output
@    tcprintu    -- print a unsigned integer number to standard output
@    tcprintsln  -- print a null-terminated ASCII string and a newline to standard output
@    tcprintiln  -- print a integer number and a newline to standard output
@    tcprintuln  -- print a unsigned integer number and a newline to standard output
@    tcprinthex  -- Output a number in hexadecimal to the standard output stream
@    tcprinthexln -- Output a number in hexadecimal and a new line to the standard output stream
@    tcgets      -- read one line from standard input
@    tcfprints   -- print a null-terminated ASCII string to a text file
@    tcfgets     -- read one line from a text file or standard input
@    tcstrlen    -- compute length of a null-terminated ASCII string
@    tcatoi      -- convert an ASCII representation of a number to an integer
@    tcitoa      -- convert an integer to a null-terminated ASCII string
@    tcutoa      -- convert an unsigned integer to a null-terminated ASCII string
@    waitcs      -- wait for a given number of centiseconds
@
@ wiringPi Functions:
@	wiringPiSetup	-- Initialises the library
@	pinMode			-- Sets the mode of a pin (only INPUT or OUTPUT)
@	digitalWrite	-- Writes the value HIGH or LOW (1 or 0) to the given pin which must have been previously set as an output
@	digitalRead		-- Returns the value read a the guiven pin (HIGH or LOW, 1 or 0)
@	delay			-- Pause the program execution for the given milliseconds (centiseconds resolution in simulator)
@	delaymicroseconds -- Pause the program execution for the given microseconds (centiseconds resolution in simulator)
@
@ Author: R.N. Horspool, June 2015 / Modificado F.Corbera, February 2017

	.global	tcprints, tcprinti, tcprintu, tcprinthex
	.global	tcprintsln, tcprintiln, tcprintuln, tcprinthexln
	.global tcgets
	.global	tcstrlen
	.global	tcitoa, tcatoi, tcutoa

	.global	wiringPiSetup, pinMode
	.global digitalWrite, digitalRead
	.global delay, delayMicroseconds, waitcs

@ TEXT SEGMENT

	.text

@ This file contains only functions; execution is not intended to begin here.
@ If control does enter here, the atoi and itoa functions are tested.
	ldr	r0, =errormsg
	bl	tcprints
	bl	runtests
	mov	r0, #0x18
	mov	r1, #0
	swi	0x123456
errormsg:
	.ascii	"Control is not expected to enter the code in this file at the top!\r\n"
	.ascii	"This file contains functions which should be called from other files.\r\n"
	.ascii	"\r\n"
	.asciz	"Entering test mode for the atoi&itoa functions ...\r\n"
	.align	2

@ prints: Output a null-terminated ASCII string to the standard output stream
@
@ Usage:
@    prints(r0)
@ Input parameter:
@    r0: the address of a null-terminated ASCII string
@ Result:
@    None, but the string is written to standard output (the console)
tcprints:
	stmfd	sp!, {r0,r1,lr}
	ldr	r1, =operands
	str	r0, [r1,#4]
	bl	tcstrlen
	str	r0, [r1,#8]
	mov	r0, #0x0
	str	r0, [r1]
	mov	r0, #0x05
	swi	0x123456
	ldmfd	sp!, {r0,r1,pc}

@ printsln: Output a null-terminated ASCII string and a newline to the standard output stream
@
@ Usage:
@    printsln(r0)
@ Input parameter:
@    r0: the address of a null-terminated ASCII string
@ Result:
@    None, but the string is written to standard output (the console)
tcprintsln:
	push {lr}
	bl tcprints
	ldr r0, =newline
	bl tcprints
	pop {pc}

@ printi: Output a integer to the standard output stream
@
@ Usage:
@    printi(r0)
@ Input parameter:
@    r0: the integer
@ Result:
@    None, but the string is written to standard output (the console)
tcprinti:
	push {r0-r1, lr}
	ldr r1, =auxbuff
	bl tcitoa
	bl tcprints
	pop {r0-r1, pc}

@ printiln: Output a integer and a newline to the standard output stream
@
@ Usage:
@    printiln(r0)
@ Input parameter:
@    r0: the integer
@ Result:
@    None, but the string is written to standard output (the console)
tcprintiln:
	push {r0-r1, lr}
	ldr r1, =auxbuff
	bl tcitoa
	bl tcprints
	ldr r0, =newline
	bl tcprints
	pop {r0-r1, pc}

@ printu: Output a unsigned integer to the standard output stream
@
@ Usage:
@    printu(r0)
@ Input parameter:
@    r0: the unsigned integer
@ Result:
@    None, but the string is written to standard output (the console)
tcprintu:
	push {r0-r1, lr}
	ldr r1, =auxbuff
	bl tcutoa
	bl tcprints
	pop {r0-r1, pc}

@ printuln: Output a unsigned integer and a newline to the standard output stream
@
@ Usage:
@    printuln(r0)
@ Input parameter:
@    r0: the unsigned integer
@ Result:
@    None, but the string is written to standard output (the console)
tcprintuln:
	push {r0-r1, lr}
	ldr r1, =auxbuff
	bl tcutoa
	bl tcprints
	ldr r0, =newline
	bl tcprints
	pop {r0-r1, pc}

@ tcprinthex: Output a number in hexadecimal to the standard output stream
@
@ Usage:
@    tcprinthex(r0)
@ Input parameter:
@    r0: the number
@ Result:
@    None, but the string is written to standard output (the console)
tcprinthex:
	push {r0-r5, lr}
	ldr r1, =hexdigit
	ldr r2, =hexstr
	add r2, r2, #2
	mov r3, #8
1:	and r4, r0, #0xf
	ldrb r5, [r1, r4]
	add r3, r3, #-1
	strb r5, [r2, r3]
	mov r0, r0, LSR #4
	cmp r3, #0
	bne 1b
	ldr r0, =hexstr
	bl tcprints
	pop {r0-r5, pc}

@ tcprinthexln: Output a number in hexadecimal to the standard output stream
@
@ Usage:
@    tcprinthex(r0)
@ Input parameter:
@    r0: the number
@ Result:
@    None, but the string is written to standard output (the console)
tcprinthexln:
	push {r0-r5, lr}
	ldr r1, =hexdigit
	ldr r2, =hexstr
	add r2, r2, #2
	mov r3, #8
1:	and r4, r0, #0xf
	ldrb r5, [r1, r4]
	add r3, r3, #-1
	strb r5, [r2, r3]
	mov r0, r0, LSR #4
	cmp r3, #0
	bne 1b
	ldr r0, =hexstr
	bl tcprintsln
	pop {r0-r5, pc}

@ fprints: Output a null-terminated ASCII string to a selected stream
@          (a text file opened for output or stdout or stderr)
@
@ Usage:
@    fprints(r0, r1)
@ Input parameters:
@    r0: a file handle
@    r1: the address of a null-terminated ASCII string
@ Result:
@    None, but the string is written to the specified stream
tcfprints:
	stmfd	sp!, {r0-r2,lr}
	ldr	r2, =operands
	str	r0, [r2]
	str	r1, [r2,#4]
	mov	r0, r1
	bl	tcstrlen
	str	r0, [r2,#8]
	mov	r1, r2
	mov	r0, #0x05
	swi	0x123456
	ldmfd	sp!, {r0-r2,pc}

@ gets: read an ASCII text line from standard input
@
@ Usage:
@    r0 = fgets(r0, r1)
@ Input parameters:
@    r0: the address of a buffer to receive the input line
@    r1: the size of the buffer (which needs to accommodate a
@        terminating null byte)
@ Result:
@    r0: the address of the buffer if some characters were read,
@        or 0 if no characters were read due to EOF or error.
@        One text line including a terminating linefeed character
@        is read into the buffer, if the buffer is large enough.
@        Otherwise the buffer holds size-1 characters and a null byte.
@        Note: the line stored in the buffer will have only a linefeed
@        (\n) line ending, even if the input source has a DOS line
@        ending (a \r\n pair).
tcgets:
	push {r1-r2, lr}
	mov r2, #0
	bl tcfgets
	pop {r1-r2, pc}

@ fgets: read an ASCII text line from an input stream (a text file
@        opened for input or standard input)
@
@ Usage:
@    r0 = fgets(r0, r1, r2)
@ Input parameters:
@    r0: the address of a buffer to receive the input line
@    r1: the size of the buffer (which needs to accommodate a
@        terminating null byte)
@    r2: the handle for a text file opened for input or 0 if input
@        should be read from stdin
@ Result:
@    r0: the address of the buffer if some characters were read,
@        or 0 if no characters were read due to EOF or error.
@        One text line including a terminating linefeed character
@        is read into the buffer, if the buffer is large enough.
@        Otherwise the buffer holds size-1 characters and a null byte.
@        Note: the line stored in the buffer will have only a linefeed
@        (\n) line ending, even if the input source has a DOS line
@        ending (a \r\n pair).
tcfgets:	stmfd	sp!, {r1-r4,lr}
	ldr	r3, =operands
	str	r2, [r3]	@ specify input stream
	mov	r2, r0
	mov	r4, r1
	mov	r0, #1
	str	r0, [r3,#8]	@ to read one character
	mov	r1, r3
	mov	r3, r2
1:	subs	r4, r4, #1
	ble	3f		@ jump if buffer has been filled
	str	r3, [r1,#4]
2:	mov	r0, #0x06	@ read operation
	swi	0x123456
	cmp	r0, #0
	bne	4f		@ branch if read failed
	ldrb	r0, [r3]
	cmp	r0, #'\r'	@ ignore \r char (result is a Unix line)
	beq	2b
	add	r3, r3, #1
	cmp	r0, #'\n'
	bne	1b
3:	mov	r0, #0
	strb	r0, [r3]
	mov	r0, r2		@ set success result
	ldmfd	sp!, {r1-r4,pc}
4:	cmp	r4, r2
	bne	3b		@ some chars were read, so return them
	mov	r0, #0		@ set failure code
	strb	r0, [r2]	@ put empty string in the buffer
	ldmfd	sp!, {r1-r4,pc}

@ strlen: compute length of a null-terminated ASCII string
@
@ Usage:
@    r0 = strlen(r0)
@ Input parameter:
@    r0: the address of a null-terminated ASCII string
@ Result:
@    r0: the length of the string (excluding the null byte at the end)
tcstrlen:
	stmfd	sp!, {r1-r3,lr}
	mov	r3, r0
	mov r0, #0
1:	ldrb	r2, [r3, r0]
	cmp	r2, #0
	addne r0, r0, #1
	bne	1b
	ldmfd	sp!, {r1-r3,pc}

@ itoa: Integer to ASCII string conversion
@
@ Usage:
@    r0 = itoa(r0, r1)
@ Input parameters:
@    r0: a signed integer
@    r1: the address of a buffer sufficiently large to hold the
@        function result, which is a null-terminated ASCII string
@ Result:
@    r0: the address of the buffer
tcitoa:
	stmfd	sp!, {r1-r7,lr}
	mov	r7, r1		@ remember buffer address
	cmp	r0, #0		@ check if negative and if zero
	movlt	r2, #'-'
	moveq	r2, #'0'
	strleb	r2, [r1],#1	@ store a '-' symbol or a '0' digit
	beq	3f
	mvnlt	r0, r0
	addlt	r0, #1	
	ldr	r3, =4f		@ R3: multiple pointer
	mov	r6, #0		@ R6: write zero digits? (no, for leading zeros)
1:	ldr	r4, [r3],#4	@ R4: current power of ten
	cmp	r4, #1		@ stop when power of ten < 1
	blt	3f
	mov	r5, #0		@ R5: multiples count
2:	subs	r0, r0, r4	@ subtract multiple from value
	addpl	r5, r5, #1	@ increment the multiples count
	bpl	2b
	add	r0, r0, r4	@ correct overshoot
	cmp	r5, #0		@ if digit is '0' and ...
	cmpeq	r6, #0		@    if it's a leading zero
	beq	1b		@ then skip it
	mov	r6, #1
	add	r2, r5, #'0'	@ ASCII code for the digit
	strb	r2, [r1],#1	@ store it
	b	1b
3:	mov	r0, #0
	strb	r0, [r1]
	mov	r0, r7
	ldmfd	sp!, {r1-r7,pc}
4:	.word	1000000000, 100000000, 10000000, 1000000
	.word	100000, 10000, 1000, 100, 10, 1, 0

@ utoa: Unsigned integer to ASCII string conversion
@
@ Usage:
@    r0 = utoa(r0, r1)
@ Input parameters:
@    r0: a signed integer
@    r1: the address of a buffer sufficiently large to hold the
@        function result, which is a null-terminated ASCII string
@ Result:
@    r0: the address of the buffer
tcutoa:
	stmfd	sp!, {r1-r7,lr}
	mov	r7, r1		@ remember buffer address
	cmp	r0, #0		@ check if zero
	moveq	r2, #'0'
	streqb	r2, [r1],#1	@ store a '0' digit
	beq	3f
	ldr	r3, =4f		@ R3: multiple pointer
	mov	r6, #0		@ R6: write zero digits? (no, for leading zeros)
1:	ldr	r4, [r3],#4	@ R4: current power of ten
	cmp	r4, #1		@ stop when power of ten < 1
	blt	3f
	mov	r5, #0		@ R5: multiples count
2:	subs	r0, r0, r4	@ subtract multiple from value
	addhs	r5, r5, #1	@ increment the multiples count
	bhs	2b
	add	r0, r0, r4	@ correct overshoot
	cmp	r5, #0		@ if digit is '0' and ...
	cmpeq	r6, #0		@    if it's a leading zero
	beq	1b		@ then skip it
	mov	r6, #1
	add	r2, r5, #'0'	@ ASCII code for the digit
	strb	r2, [r1],#1	@ store it
	b	1b
3:	mov	r0, #0
	strb	r0, [r1]
	mov	r0, r7
	ldmfd	sp!, {r1-r7,pc}
4:	.word	1000000000, 100000000, 10000000, 1000000
	.word	100000, 10000, 1000, 100, 10, 1, 0

@ atoi: ASCII string to integer conversion
@
@ Usage:
@    r0 = atoi(r0)
@ Input parameters:
@    r0: the address of a null-terminated ASCII string
@ Result:
@    r0: the value of the converted integer
tcatoi:
	stmfd	sp!, {r1-r4,lr}
	mov	r2, #0		@ holds result
	mov	r3, #0		@ set to 1 if a negative number
	mov	r4, #10
1:	ldrb	r1, [r0], #1	@ get next char
	cmp	r1, #0
	beq	4f
	cmp	r1, #' '
	beq	1b
	cmp	r1, #'-'
	moveq	r3, #1
	ldreqb	r1, [r0], #1
	b	3f
2:	cmp	r1, #9
	bgt	4f
	mul	r2, r4, r2
	add	r2, r2, r1
	ldrb	r1, [r0], #1
3:	subs	r1, r1, #'0'
	bge	2b
4:	cmp	r3, #0
	moveq	r0, r2
	mvnne	r0, r2
	addne	r0, #1
	ldmfd	sp!, {r1-r4,pc}

@ waitcs: wait centiseconds
@
@ Input parameters:
@    r0: number of centiseconds to wait
@ Result:
@    None
waitcs:
	push {r0-r3, lr}
	mov r1, r0
	mov r0, #0x10
	swi 0x123456
	add r1, r1, r0
1:	mov r0, #0x10
	swi 0x123456
	cmp r1, r0
	bhi 1b
	pop {r0-r3, pc}


@ wiringPi Functions

@	wiringPiSetup	-- Initialises the library
@
@ Usage:
@	wiringPiSetup()
@ Input parameters:
@    None
@ Result:
@    None

wiringPiSetup:
	mov r0, #1
	ldr r1, =wiringinit
	str r0, [r1]
	bx lr

@	pinMode			-- Sets the mode of a pin (only INPUT or OUTPUT)
@
@ Usage:
@    pinMode(r0, r1)
@ Input parameters:
@    r0: pin number
@	 r1: mode
@ Result:
@    None
pinMode:
	push {r0-r5, lr}
	mov r3, r0
	bl checklib
	cmp r0, #0
	beq 1f
	mov r0, r3
	mov r4, r0
	mov r5, r1
	mov r2, r1
	mov r1, r0
	ldr r3, =gpiomode
	ldr r0, [r3]
	bl setbit
	ldr r3, =gpiomode
	str r0, [r3]
	ldr r3, =gpioinit
	ldr r0, [r3]
	mov r1, r4
	mov r2, #1
	bl setbit
	ldr r3, =gpioinit
	str r0, [r3]
1:	pop {r0-r5, pc}

@	digitalWrite	-- Writes the value HIGH or LOW (1 or 0) to the given pin which must have been previously set as an output
@
@ Usage:
@    digitalWrite(r0, r1)
@ Input parameters:
@    r0: pin number
@	 r1: value
@ Result:
@    None
digitalWrite:
	push {r0-r6, lr}
	mov r4, r0
	mov r5, r1
	mov r1, #OUTPUT
	bl checkpin			@ chequeamos que libreria y pin esten inicializados y como output
	cmp r0, #0
	beq 2f
1:	ldr r0, =gpio
	ldr r0, [r0]
	mov r1, r4
	and r2, r5, #1
	bl setbit
	ldr r1, =gpio
	str r0, [r1]

	ldr r5, =leds		
	mov r6, #0		
	
3:	ldr r1, [r5, r6, LSL #2]		
	cmp r1, r4
	beq 4f
	add r6, r6, #1		
	cmp r6, #6			
	bne 3b			
	b 2f
4:	bl printledstatus
2:	pop {r0-r6, pc}

@	digitalRead		-- Returns the value read a the guiven pin (HIGH or LOW, 1 or 0)
digitalRead:
	bx lr

@	delay			-- Pause the program execution for the given milliseconds (centiseconds resolution in simulator)
@
@ Usage:
@    delay(r0)
@ Input parameters:
@    r0: number of milliseconds
@ Result:
@    None
delay:
	push {r0-r3, lr}
	mov r3, r0
	bl checklib
	cmp r0, #0
	beq 1f
	mov r0, r3
	mov r0, r0, LSR #3
	bl waitcs
1:	pop {r0-r3, pc}

@	delayMicroseconds -- Pause the program execution for the given microseconds (centiseconds resolution in simulator)
delayMicroseconds:
	push {r0, lr}
	bl checklib
	pop {r0, pc}

@	checklib -- Chequea que la libreria se ha inicializado
@ Usage:
@    r0 = checklib()
@ Input parameters:
@    None
@ Result:
@    r0: 1 if everyting is ok, 0 o.c.
checklib:
	push {lr}
	ldr r0, =wiringinit		
	ldr r0, [r0]
	cmp r0, #0
	bne 1f
	ldr r0, =werr1		@ si falla imprimimos mensaje de error
	bl tcprintsln
	mov r0, #0
1:	pop {pc}

@	checkpin -- Chequea que un pin se puede usar como se pretende
@ Usage:
@    r0 = checkpin(r0, r1)
@ Input parameters:
@    r0: pin number
@	 r1: access mode (0 input, 1 output)
@ Result:
@    r0: 1 if everyting is ok, 0 o.c.
checkpin:
	push {r1-r5, lr}
	mov r4, r0
	mov r5, r1
	bl checklib
	cmp r0, #0
	moveq r0, #0
	beq 1f
	ldr r2, =gpioinit		@ comprobamos si el pin concreto (r0) se ha inicializado
	ldr r2, [r2]			@ gpioinit el bit correspondiente = 1?
	mov r2, r2, LSR r4
	tst r2, #1
	bne 2f
	ldr r0, =werr2
	bl tcprintsln
	mov r0, #0
	b 1f
2:	ldr r2, =gpiomode		@ comprobamos que el modo de dicho pin (r0) es el que me indican (r1)
	ldr r2, [r2]			@ gpiomode el bit correspondiente = r1 (r5)?
	mov r2, r2, LSR r4
	and r2, r2, #1
	and r5, r5, #1
	cmp r5, r2
	moveq r0, #1
	beq 1f
	ldr r0, =werr3
	bl tcprintsln
	mov r0, #0
1:	pop {r1-r5, pc}

@	setbit -- establece el valor de un bit determinado de un word
@
@ Usage:
@    r0 = setbit(r0, r1, r2)
@ Input parameters:
@    r0: word
@	 r1: bit number
@	 r2: value
@ Result:
@    r0: modified word
setbit:
	ands r2, r2, #1
	mov r2, #1 
	mov r2, r2, LSL r1
	orrne r0, r0, r2
	mvneq r2, r2
	andeq r0, r0, r2
	bx lr

@	getbit -- devuelve el valor de un bit determinado de un word
@
@ Usage:
@    r0 = getbit(r0, r1)
@ Input parameters:
@    r0: word
@	 r1: bit number
@ Result:
@    r0: bit status (0/1)
getbit:
	mov r0, r0, LSR r1
	and r0, r0, #1
	bx lr

@ printledstatus -- imprimer por salida estandar el estado de los leds
@
@ Usage:
@	printledstatus()
@ Input parameters:
@	None
@ Result:
@	None
printledstatus:
	push {r0-r6, lr}
	ldr r1, =gpio
	ldr r4, [r1]
	ldr r5, =leds		
	mov r6, #0		
	
1:	ldr r1, [r5, r6, LSL #2]		
	mov r0, r4		
	bl getbit

	cmp r0, #0
	ldreq r1, =ledstr0
	ldrne r1, =ledstr1
	add r0, r1, r6, LSL #1
	bl tcprints
						
	add r6, r6, #1		
	cmp r6, #6			
	bne 1b			

	ldr r0, =newline
	bl tcprints

	pop {r0-r6, pc}

@ test function, invoked if control enters at top of the file
runtests:
	stmfd	sp!, {r0-r3,lr}
	ldr	r0, =testmsg0
	bl	tcprints
	ldr	r2, =testnums
1:	ldr	r1, =testbuff
	ldr	r0, [r2], #4	@ load a test value
2:	mov	r3, r0		@ keep a copy in r3
	bl	tcitoa		@ convert to ASCII
	ldr	r0, =testmsg1
	bl	tcprints
	ldr	r0, =testbuff	@ print the ASCII version of the number
	bl	tcprints
	ldr	r0, =testbuff
	bl	tcatoi		@ convert the ASCII back to a number
	cmp	r0, r3		@ get back the same number?
	ldreq	r0, =testcrlf	@ yes -> an empty message
	ldrne	r0, =testmsg2	@ no  -> an error message
	bl	tcprints		@ print the message
	cmp	r3, #0
	blt	1b		@ repeat with a new test integer
	mvn	r0, r3
	bgt	2b		@ repeat test with negated number
	ldr	r0, =testmsg3
	bl	tcprints
	ldmfd	sp!, {r0-r3,pc}
@ test values: the end of the list is indicated by a 0 value
testnums:
	.word	9, 123, 546781, 2147483647, 0
testmsg0:
	.asciz	"Testing itoa/atoi in pairs...\n"
testmsg1:
	.asciz	"- Number being tested: "
testmsg2:
	.asciz	" *** TEST FAILED!\r\n"
testmsg3:
	.asciz	"Testing of itoa/atoi completed\r\n"
testcrlf:
	.asciz	"\r\n"
werr1:
	.asciz	"Libreria no inicializada"
werr2:
	.asciz	"Pin no inicializado"
werr3:
	.asciz	"Modo incorrecto en el pin"
@ DATA SEGMENT
	.data
operands:
	.word	0, 0, 0
testbuff:
	.space	16
auxbuff:	
	.space	256	
hexdigit:
	.ascii "0123456789ABCDEF"
hexstr:
	.asciz "0x00000000"

.balign 4
newline: 
	.asciz "\n"

@wiringPi variables
.set    BUTTON1,8       /* button 1 */
.set    BUTTON2,9       /* button 2 */
.set    BUZZER, 7       /* buzzer */
.set    RLED1,  13      /* red led 1 */
.set    RLED2,  12      /* red led 2 */
.set    YLED1,  14      /* yellow led 1 */
.set    YLED2,  0       /* yellow led 2 */
.set    GLED1,  3       /* green led 1 */
.set    GLED2,  2       /* green led 2 */

.set    INPUT,  0       /* to set pin as input */
.set    OUTPUT, 1       /* to set pin as output */

.balign 4
gpio:					
	.word 0 			@ status of each gpio (0..31) 
gpioinit:
	.word 0             @ if a gpio (0..31) has been initialised (1)
gpiomode:
	.word 0 			@ mode of each gpio (0..31) (0 input, 1 output)
wiringinit:
	.word 0 			@ Does the library has been initialised?
leds:	
	.word RLED1, RLED2, YLED1, YLED2, GLED1, GLED2
ledstr1:
	.byte 'R',0,'R',0,'Y',0,'Y',0,'G',0,'G',0
ledstr0:
	.byte '-',0,'-',0,'-',0,'-',0,'-',0,'-',0
	
