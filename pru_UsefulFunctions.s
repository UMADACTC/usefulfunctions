@ pru_UsefulFunctions.s
@
@ Fichero de ejemplo de uso de la libreria UsefulFunctions.s / UsefulFunctionsRpi.s
@
@ Uso con el simulador ARMSim#:
@	- Cargar en el simulador (File->Load Multiple) los ficheros pru_UsefulFunctions y UsefulFunctions.s
@	- Ejecutar normalmente
@
@ Uso en la Raspberry Pi:
@	- Ensamblar y enlazar fichero y librería:
@		as pru_UsefulFunctions.s -o pru_UsefulFunctions.o
@		as UsefulFunctionsRpi.s -o UsefulFunctionsRpi.o
@		gcc pru_UsefulFunctions.o UsefulFunctionsRpi.o -o pru_UsefulFunctions
@	- Ejecutar:
@		./pru_UsefulFunctions

.data
num:  	.word 0xffffffff
cad0:	.asciz "Comenzando con las pruebas de la libreria UsefulFunctions ..."
cad1:	.asciz "Este es el numero con signo: "
cad2:	.asciz " y este es el numero sin signo: "
cad3:	.asciz "Escribe un numero de 2 cifras: "
cad4:	.asciz "\nEl numero que has escrito menos uno es: "
cad5:	.asciz "Su representacion hexadecimal es: "
buff:	.space 256

.text
.global main	
main:
	push {lr}	

	ldr r0, =cad0	@ cargamos r0 con la direccion de cad0
	bl tcprintsln	@ imprimimos cad0 con newline al final

	ldr r0, =cad1	@ cargamos r0 con la direccion de cad1
	bl tcprints 	@ la imprimimos sin newline al final
	ldr r0, =num 	@ cargamos la direccion de la variable entera num
	ldr r0, [r0]	@ cargamos en r0 el valor de la variable (0xffffffff)
	bl tcprinti 	@ la imprimimos como un numero con signo (-1) (sin newline)
	ldr r0, =cad2   @ cargamos en r0 la direccion de cad2
	bl tcprints 	@ la imprimimos sin newline al final
	ldr r0, =num 	@ volvemos a cargar la direccion de la variable num
	ldr r0, [r0] 	@ y volvemos a cargar en r0 el valor de la variable (0xffffffff)
	bl tcprintuln 	@ pero ahora lo imprimimos como un numero sin signo y con newline (esto termina la linea)

	ldr r0, =cad3 	
	bl tcprints 	@ imprimimos cad3 sin newline

	ldr r0, =buff 	@ en r0 cargamos la direccion del buffer donde almacenar respuesta leida por teclado
	mov r1, #3	@ en r1 ponemos el tamaño del buffer
	bl tcgets 		@ leemos una cadena de la entrada estandar (en buff)

	ldr r0, =buff 	@ tomamos la direccion del buffer con la cadena leida
	bl tcatoi		@ la transformamos a un entero
	sub r1, r0, #1 	@ le restamos 1

	ldr r0, =cad4
	bl tcprints 	@ imprimimos cad4 sin newline

	mov r0, r1		@ ponemos en r0 el numero leido al que le hemos restado 1
	bl tcprintiln 	@ y lo imprimimos como entero y terminando la linea

	mov r4, r0		@ guardamos el numero en r4 para no perderlo en la siguiente llamada

	ldr r0, =cad5
	bl tcprints 	@ imprimimos cad5 sin newline

	mov r0, r4		@ recuperamos el numero guardado en r4 y lo ponemos en r0 para la llamada
	bl tcprinthexln @ imprimimos el numero en formato hexadecimal

	pop {pc}
