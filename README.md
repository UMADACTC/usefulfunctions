# README #

Librería con funciones en ensamblador ARM para la entrada/salida por consola, basada en la de R.N. Horspool (ARMSim#),
añadiendo algunas funciones adicionales.

Hay una versión para ser utilizada con el simulador ARMSim# (UsefulFunctions.s) y otra para usar en la RaspberryPi (UsefulFunctionsRpi.s)

### ¿Para qué es este repositorio? ###

* Estas librerías están enfocadas a facilitar la entrada/salida por consola a los alumnos de primer curso de la asignatura de Tecnología de Computadores de los Gados de Informática de la Universidad de Málaga
* Version 0.1

### ¿Como empezar a utilizar la librería? ###

Uso con el simulador ARMSim#:

* Cargar con "File->Load multiple" el programa ".s" y el fichero UsefulFunctions.s
* Ejecutar con el simulador el programa con las llamadas a la libreria

Uso en la Raspberry Pi: 

* Ensamblar: as UsefulFunctionsRpi.s -o UsefulFunctionsRpi.o
* Enlazar con un programa: gcc programa.o UsefulFunctionsRpi.o -o ejecutable
* Ejecutar: ./ejecutable

Functions:

* tcprints    -- print a null-terminated ASCII string to standard output
* tcprinti    -- print a integer number to standard output
* tcprintu    -- print a unsigned integer number to standard output
* tcprintsln  -- print a null-terminated ASCII string and a newline to standard output
* tcprintiln  -- print a integer number and a newline to standard output
* tcprintuln  -- print a unsigned integer number and a newline to standard output
* tcprinthex  -- Output a number in hexadecimal to the standard output stream
* tcprinthexln -- Output a number in hexadecimal and a new line to the standard output stream
* tcgets      -- read one line from standard input
* tcstrlen    -- compute length of a null-terminated ASCII string
* tcatoi      -- convert an ASCII representation of a number to an integer
* tcitoa      -- convert an integer to a null-terminated ASCII string
* tcutoa      -- convert an unsigned integer to a null-terminated ASCII string