.data
tam:	.word 8
datos:	.word 2, 4, 6, 8, -2, -4, -6, -7
res:	.word 0
text:	.asciz "La suma de los elementos del vector es "

.text
.global main
main:	push {lr}
		ldr r0, =tam
		ldr r1, [r0]
		ldr r2, =datos
		mov r3, #0
loop:	cmp r1, #0
		beq salir
		ldr r4, [r2], #4
		add r3, r3, r4
		sub r1, #1
		b loop
salir:	ldr r0, =res
		str r3, [r0]
		mov r0, r3
		mov r4, r0		@ guardamos el maximo en r4 para no perderlo en la siguiente llamada
		ldr r0, =text	@ cargamos en r0 la direccion del mensaje a mostrar
		bl tcprints		@ llamamos a la funcion para mostrar un texto
		mov r0, r4		@ volvemos a cargar en r0 el valor maximo
		bl tcprintiln	@ llamamos a la funcion para mostrar un entero y newline
		pop {pc}

