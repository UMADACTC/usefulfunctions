@ Libreria con funciones utiles para imprimir por pantalla y leer de teclado en la 
@ Raspberry Pi.
@
@ Uso: 
@	Ensamblar: as UsefulFunctionsRpi.s -o UsefulFunctionsRpi.o
@	Enlazar con un programa: gcc programa.o UsefulFunctionsRpi.o -o ejecutable
@	Ejecutar: ./ejecutable
@
@ Functions:
@    tcprints    -- print a null-terminated ASCII string to standard output
@    tcprinti    -- print a integer number to standard output
@    tcprintu    -- print a unsigned integer number to standard output
@    tcprintsln  -- print a null-terminated ASCII string and a newline to standard output
@    tcprintiln  -- print a integer number and a newline to standard output
@    tcprintuln  -- print a unsigned integer number and a newline to standard output
@    tcprinthex  -- Output a number in hexadecimal to the standard output stream
@    tcprinthexln -- Output a number in hexadecimal and a new line to the standard output stream
@    tcgets      -- read one line from standard input
@    tcstrlen    -- compute length of a null-terminated ASCII string
@    tcatoi      -- convert an ASCII representation of a number to an integer
@    tcitoa      -- convert an integer to a null-terminated ASCII string
@    tcutoa      -- convert an unsigned integer to a null-terminated ASCII string
@
@ Author: F.Corbera, February 2017

	.global	tcprints, tcprinti, tcprintu, tcprinthex
	.global	tcprintsln, tcprintiln, tcprintuln, tcprinthexln
	.global tcgets
	.global	tcstrlen
	.global	tcitoa, tcatoi, tcutoa

@ TEXT SEGMENT

	.text

@ This file contains only functions; execution is not intended to begin here.
@ If control does enter here, the atoi and itoa functions are tested.
	push {lr}
	ldr	r0, =errormsg
	bl	tcprints
	bl	runtests
	mov	r0, #0
	pop {pc}
errormsg:
	.ascii	"Control is not expected to enter the code in this file at the top!\r\n"
	.ascii	"This file contains functions which should be called from other files.\r\n"
	.ascii	"\r\n"
	.asciz	"Entering test mode for the atoi&itoa functions ...\r\n"
	.align	2

@ prints: Output a null-terminated ASCII string to the standard output stream
@
@ Usage:
@    prints(r0)
@ Input parameter:
@    r0: the address of a null-terminated ASCII string
@ Result:
@    None, but the string is written to standard output (the console)
tcprints:
	stmfd	sp!, {r0,r1,lr}
	mov r1, r0
	ldr r0, =msgstr
	bl printf
	ldmfd	sp!, {r0,r1,pc}

@ printsln: Output a null-terminated ASCII string and a newline to the standard output stream
@
@ Usage:
@    printsln(r0)
@ Input parameter:
@    r0: the address of a null-terminated ASCII string
@ Result:
@    None, but the string is written to standard output (the console)
tcprintsln:
	stmfd	sp!, {r0,r1,lr}
	mov r1, r0
	ldr r0, =msgstrnl
	bl printf
	ldmfd	sp!, {r0,r1,pc}

@ printi: Output a integer to the standard output stream
@
@ Usage:
@    printi(r0)
@ Input parameter:
@    r0: the integer
@ Result:
@    None, but the string is written to standard output (the console)
tcprinti:
	stmfd	sp!, {r0,r1,lr}
	mov r1, r0
	ldr r0, =msgint
	bl printf
	ldmfd	sp!, {r0,r1,pc}

@ printiln: Output a integer and a newline to the standard output stream
@
@ Usage:
@    printiln(r0)
@ Input parameter:
@    r0: the integer
@ Result:
@    None, but the string is written to standard output (the console)
tcprintiln:
	stmfd sp!, {r0,r1,lr}
	mov r1, r0
	ldr r0, =msgintnl
	bl printf
	ldmfd sp!, {r0,r1,pc}

@ printu: Output a unsigned integer to the standard output stream
@
@ Usage:
@    printu(r0)
@ Input parameter:
@    r0: the unsigned integer
@ Result:
@    None, but the string is written to standard output (the console)
tcprintu:
	stmfd	sp!, {r0,r1,lr}
	mov r1, r0
	ldr r0, =msguint
	bl printf
	ldmfd	sp!, {r0,r1,pc}

@ printuln: Output a unsigned integer and a newline to the standard output stream
@
@ Usage:
@    printuln(r0)
@ Input parameter:
@    r0: the unsigned integer
@ Result:
@    None, but the string is written to standard output (the console)
tcprintuln:
	stmfd	sp!, {r0,r1,lr}
	mov r1, r0
	ldr r0, =msguintnl
	bl printf
	ldmfd	sp!, {r0,r1,pc}


@ tcprinthex: Output a number in hexadecimal to the standard output stream
@
@ Usage:
@    tcprinthex(r0)
@ Input parameter:
@    r0: the number
@ Result:
@    None, but the string is written to standard output (the console)
tcprinthex:
	push {r0-r5, lr}
	ldr r1, =hexdigit
	ldr r2, =hexstr
	add r2, r2, #2
	mov r3, #8
1:	and r4, r0, #0xf
	ldrb r5, [r1, r4]
	add r3, r3, #-1
	strb r5, [r2, r3]
	mov r0, r0, LSR #4
	cmp r3, #0
	bne 1b
	ldr r0, =hexstr
	bl tcprints
	pop {r0-r5, pc}

@ tcprinthexln: Output a number in hexadecimal to the standard output stream
@
@ Usage:
@    tcprinthex(r0)
@ Input parameter:
@    r0: the number
@ Result:
@    None, but the string is written to standard output (the console)
tcprinthexln:
	push {r0-r5, lr}
	ldr r1, =hexdigit
	ldr r2, =hexstr
	add r2, r2, #2
	mov r3, #8
1:	and r4, r0, #0xf
	ldrb r5, [r1, r4]
	add r3, r3, #-1
	strb r5, [r2, r3]
	mov r0, r0, LSR #4
	cmp r3, #0
	bne 1b
	ldr r0, =hexstr
	bl tcprintsln
	pop {r0-r5, pc}

@ gets: read an ASCII text line from standard input
@
@ Usage:
@    r0 = fgets(r0, r1)
@ Input parameters:
@    r0: the address of a buffer to receive the input line
@    r1: the size of the buffer (which needs to accommodate a
@        terminating null byte)
@ Result:
@    r0: the address of the buffer if some characters were read,
@        or 0 if no characters were read due to EOF or error.
@        One text line including a terminating linefeed character
@        is read into the buffer, if the buffer is large enough.
@        Otherwise the buffer holds size-1 characters and a null byte.
@        Note: the line stored in the buffer will have only a linefeed
@        (\n) line ending, even if the input source has a DOS line
@        ending (a \r\n pair).
tcgets:
	push {r1-r6, lr}
	mov r4, r0
	mov r5, r1
	mov r6, #0
3:	cmp r6, r5
	beq 1f
	bl getchar
	cmp r0, #'\n'
	beq 2f
	strb r0, [r4, r6]
	add r6, r6, #1
	b 3b	
2:	mov r0, #0
	cmp r6, #0
	beq 4f
	strb r0, [r4, r6]
	mov r0, r4
	b 4f	
1:	mov r0, #0
	subs r6, r6, #1
	strplb r0, [r4, r6]
	movpl r0, r4
	movmi r0, #0
4:	pop {r1-r6, pc}

@ strlen: compute length of a null-terminated ASCII string
@
@ Usage:
@    r0 = strlen(r0)
@ Input parameter:
@    r0: the address of a null-terminated ASCII string
@ Result:
@    r0: the length of the string (excluding the null byte at the end)
tcstrlen:
	stmfd	sp!, {r1-r3,lr}
	mov	r3, r0
	mov r0, #0
1:	ldrb	r2, [r3, r0]
	cmp	r2, #0
	addne r0, r0, #1
	bne	1b
	ldmfd	sp!, {r1-r3,pc}

@ itoa: Integer to ASCII string conversion
@
@ Usage:
@    r0 = itoa(r0, r1)
@ Input parameters:
@    r0: a signed integer
@    r1: the address of a buffer sufficiently large to hold the
@        function result, which is a null-terminated ASCII string
@ Result:
@    r0: the address of the buffer
tcitoa:
	stmfd	sp!, {r1-r7,lr}
	mov	r7, r1		@ remember buffer address
	cmp	r0, #0		@ check if negative and if zero
	movlt	r2, #'-'
	moveq	r2, #'0'
	strleb	r2, [r1],#1	@ store a '-' symbol or a '0' digit
	beq	3f
	mvnlt	r0, r0
	addlt	r0, #1	
	ldr	r3, =4f		@ R3: multiple pointer
	mov	r6, #0		@ R6: write zero digits? (no, for leading zeros)
1:	ldr	r4, [r3],#4	@ R4: current power of ten
	cmp	r4, #1		@ stop when power of ten < 1
	blt	3f
	mov	r5, #0		@ R5: multiples count
2:	subs	r0, r0, r4	@ subtract multiple from value
	addpl	r5, r5, #1	@ increment the multiples count
	bpl	2b
	add	r0, r0, r4	@ correct overshoot
	cmp	r5, #0		@ if digit is '0' and ...
	cmpeq	r6, #0		@    if it's a leading zero
	beq	1b		@ then skip it
	mov	r6, #1
	add	r2, r5, #'0'	@ ASCII code for the digit
	strb	r2, [r1],#1	@ store it
	b	1b
3:	mov	r0, #0
	strb	r0, [r1]
	mov	r0, r7
	ldmfd	sp!, {r1-r7,pc}
4:	.word	1000000000, 100000000, 10000000, 1000000
	.word	100000, 10000, 1000, 100, 10, 1, 0

@ utoa: Unsigned integer to ASCII string conversion
@
@ Usage:
@    r0 = utoa(r0, r1)
@ Input parameters:
@    r0: a signed integer
@    r1: the address of a buffer sufficiently large to hold the
@        function result, which is a null-terminated ASCII string
@ Result:
@    r0: the address of the buffer
tcutoa:
	stmfd	sp!, {r1-r7,lr}
	mov	r7, r1		@ remember buffer address
	cmp	r0, #0		@ check if zero
	moveq	r2, #'0'
	streqb	r2, [r1],#1	@ store a '0' digit
	beq	3f
	ldr	r3, =4f		@ R3: multiple pointer
	mov	r6, #0		@ R6: write zero digits? (no, for leading zeros)
1:	ldr	r4, [r3],#4	@ R4: current power of ten
	cmp	r4, #1		@ stop when power of ten < 1
	blt	3f
	mov	r5, #0		@ R5: multiples count
2:	subs	r0, r0, r4	@ subtract multiple from value
	addhs	r5, r5, #1	@ increment the multiples count
	bhs	2b
	add	r0, r0, r4	@ correct overshoot
	cmp	r5, #0		@ if digit is '0' and ...
	cmpeq	r6, #0		@    if it's a leading zero
	beq	1b		@ then skip it
	mov	r6, #1
	add	r2, r5, #'0'	@ ASCII code for the digit
	strb	r2, [r1],#1	@ store it
	b	1b
3:	mov	r0, #0
	strb	r0, [r1]
	mov	r0, r7
	ldmfd	sp!, {r1-r7,pc}
4:	.word	1000000000, 100000000, 10000000, 1000000
	.word	100000, 10000, 1000, 100, 10, 1, 0

@ atoi: ASCII string to integer conversion
@
@ Usage:
@    r0 = atoi(r0)
@ Input parameters:
@    r0: the address of a null-terminated ASCII string
@ Result:
@    r0: the value of the converted integer
tcatoi:
	stmfd	sp!, {r1-r4,lr}
	mov	r2, #0		@ holds result
	mov	r3, #0		@ set to 1 if a negative number
	mov	r4, #10
1:	ldrb	r1, [r0], #1	@ get next char
	cmp	r1, #0
	beq	4f
	cmp	r1, #' '
	beq	1b
	cmp	r1, #'-'
	moveq	r3, #1
	ldreqb	r1, [r0], #1
	b	3f
2:	cmp	r1, #9
	bgt	4f
	mul	r2, r4, r2
	add	r2, r2, r1
	ldrb	r1, [r0], #1
3:	subs	r1, r1, #'0'
	bge	2b
4:	cmp	r3, #0
	moveq	r0, r2
	mvnne	r0, r2
	addne	r0, #1
	ldmfd	sp!, {r1-r4,pc}

@ test function, invoked if control enters at top of the file
runtests:
	stmfd	sp!, {r0-r3,lr}
	ldr	r0, =testmsg0
	bl	tcprints
	ldr	r2, =testnums
1:	ldr	r1, =testbuff
	ldr	r0, [r2], #4	@ load a test value
2:	mov	r3, r0		@ keep a copy in r3
	bl	tcitoa		@ convert to ASCII
	ldr	r0, =testmsg1
	bl	tcprints
	ldr	r0, =testbuff	@ print the ASCII version of the number
	bl	tcprints
	ldr	r0, =testbuff
	bl	tcatoi		@ convert the ASCII back to a number
	cmp	r0, r3		@ get back the same number?
	ldreq	r0, =testcrlf	@ yes -> an empty message
	ldrne	r0, =testmsg2	@ no  -> an error message
	bl	tcprints		@ print the message
	cmp	r3, #0
	blt	1b		@ repeat with a new test integer
	mvn	r0, r3
	bgt	2b		@ repeat test with negated number
	ldr	r0, =testmsg3
	bl	tcprints
	ldmfd	sp!, {r0-r3,pc}
@ test values: the end of the list is indicated by a 0 value
testnums:
	.word	9, 123, 546781, 2147483647, 0
testmsg0:
	.asciz	"Testing itoa/atoi in pairs...\n"
testmsg1:
	.asciz	"- Number being tested: "
testmsg2:
	.asciz	" *** TEST FAILED!\r\n"
testmsg3:
	.asciz	"Testing of itoa/atoi completed\r\n"
testcrlf:
	.asciz	"\r\n"

@ DATA SEGMENT
	.data
operands:
	.word	0, 0, 0
testbuff:
	.space	16
auxbuff:	
	.space	256
hexdigit:
	.ascii "0123456789ABCDEF"
hexstr:
	.asciz "0x00000000"
newline: 
	.asciz "\n"
msgstr:
	.asciz "%s"
msgstrnl:
	.asciz "%s\n"
msgint:
	.asciz "%d"
msgintnl:
	.asciz "%d\n"
msguint:
	.asciz "%u"
msguintnl:
	.asciz "%u\n"
	
